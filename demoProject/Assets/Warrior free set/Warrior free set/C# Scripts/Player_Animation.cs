using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Animation : MonoBehaviour
{
    public GameObject myPlayer;
    public Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckPlayerInput();
    }
    void CheckPlayerInput()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Attack();
        }
        if(Input.GetKeyDown("a") || Input.GetKeyDown("d"))
        {
            Run();
        }
        else { StopRunning(); }
    }
    void Attack()
    {
        animator.SetTrigger("Attack");
    }

    void Run()
    {
        animator.SetInteger("Speed", 1);
    }
    void StopRunning()
    {
        animator.SetInteger("Speed", 0);
    }
}
